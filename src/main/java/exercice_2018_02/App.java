package exercice_2018_02;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class App implements AppInterface {

	private final static char WATER = '~';

	public static void main(String[] args) throws IOException {

		App app = new App();
		char[][] grid = new char[10][10];

		Method[] methods = GridGeneratorWithErrors.class.getMethods();

		for (Method method : methods) {
			if (method.getName().startsWith("generate")) {
				for (int i = 0; i < 200; i++) {
					String input = method.getName() + "/" + String.format("%03d", i) + ".csv";
					grid = Utils.fromFileToMatrix(input);
					boolean checkHasRightWaterAmount = app.checkHasRightWaterAmount(grid);
					boolean checkEachBoatHasRightAmount = app.checkEachBoatHasRightAmount(grid);
					boolean checkEachBoatInExactlyOneRowOrColumnAndAdjacent = app
							.checkEachBoatInExactlyOneRowOrColumnAndAdjacent(grid);
					if (checkHasRightWaterAmount && checkEachBoatHasRightAmount
							&& checkEachBoatInExactlyOneRowOrColumnAndAdjacent) {
						System.out.println(input + ": isValid");
					} else {
						System.out.println(input + ": isNotValid (" + checkHasRightWaterAmount + " water; "
								+ checkEachBoatHasRightAmount + " boats amount; "
								+ checkEachBoatInExactlyOneRowOrColumnAndAdjacent + " boats location)");
					}
				}
			}
		}
	}

	@Override
	public boolean checkGrid(char[][] grid) {
		return checkHasRightWaterAmount(grid) && checkEachBoatHasRightAmount(grid)
				&& checkEachBoatInExactlyOneRowOrColumnAndAdjacent(grid);
	}

	public boolean checkHasRightWaterAmount(char[][] grid) {
		int waterTiles = countChar(WATER, grid);
		// 10x10 grid - 5p - 4c - 3s - 3o - 2t = 83 !
		return waterTiles == 83;
	}

	public boolean checkEachBoatHasRightAmount(char[][] grid) {
		Map<Character, Integer> boats = getBoats();
		for (char boat : boats.keySet()) {
			int boatCells = countChar(boat, grid);
			if (boatCells != boats.get(boat)) {
				return false;
			}
		}
		return true;
	}

	public boolean checkEachBoatInExactlyOneRowOrColumnAndAdjacent(char[][] grid) {
		Map<Character, Integer> boats = getBoats();
		for (char boat : boats.keySet()) {
			int[] rowCol = findFirstCell(boat, grid);
			if (rowCol[0] == -1 || rowCol[1] == -1) {
				return false;
			}
			boolean tryEast = tryEast(grid, boat, boats.get(boat), rowCol[0], rowCol[1]);
			boolean trySouth = trySouth(grid, boat, boats.get(boat), rowCol[0], rowCol[1]);
			if (!tryEast && !trySouth) {
				return false;
			}
		}
		return true;
	}

	public Map<Character, Integer> getBoats() {
		Map<Character, Integer> boats = new HashMap<Character, Integer>();
		boats.put('p', 5);
		boats.put('c', 4);
		boats.put('o', 3);
		boats.put('s', 3);
		boats.put('t', 2);
		return boats;
	}

	private int[] findFirstCell(char boat, char[][] grid) {
		int[] result = new int[] { -1, -1 };
		for (int row = 0; row < grid.length; row++) {
			char[] cs = grid[row];
			for (int col = 0; col < cs.length; col++) {
				char c = cs[col];
				if (c == boat) {
					result[0] = row;
					result[1] = col;
					return result;
				}
			}
		}
		return result;
	}

	private int countChar(char water2, char[][] grid) {
		int result = 0;
		for (int i = 0; i < grid.length; i++) {
			char[] cs = grid[i];
			for (int j = 0; j < cs.length; j++) {
				char c = cs[j];
				if (c == water2) {
					result++;
				}
			}
		}
		return result;
	}

	private static boolean tryEast(char[][] grid, char boat, int vesselLength, int row, int col) {
		try {
			// We already know there is a 'boat' char at [row][col]
			for (int i = col + 1; i <= col + vesselLength - 1; i++) {
				if (grid[row][i] != boat) {
					return false;
				}
			}
			return true;
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}

	private static boolean trySouth(char[][] grid, char boat, int vesselLength, int row, int col) {
		try {
			// We already know there is a 'boat' char at [row][col]
			for (int i = row + 1; i <= row + vesselLength - 1; i++) {
				if (grid[i][col] != boat) {
					return false;
				}
			}
			return true;
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}
}
