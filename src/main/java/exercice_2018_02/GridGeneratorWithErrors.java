package exercice_2018_02;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class GridGeneratorWithErrors {

	private final char WATER = '~';

	public static void main(String[] args)
			throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		char[][] grid = new char[10][10];

		GridGeneratorWithErrors gridGeneratorWithErrors = new GridGeneratorWithErrors();

		Method[] methods = gridGeneratorWithErrors.getClass().getMethods();

		for (Method method : methods) {
			if (method.getName().startsWith("generate")) {
				File file = new File(method.getName());
				file.mkdirs();
				for (int i = 0; i < 200; i++) {
					method.invoke(gridGeneratorWithErrors, grid);
					gridGeneratorWithErrors.fromMatrixToFile(grid,
							"inputs/" + method.getName() + "_" + String.format("%03d", i) + ".csv");
				}

			}
		}
	}

	public void generateValidGrid(char[][] grid) {
		// Fill grid with WATER!
		initGrid(grid);

		Map<Character, Integer> boats = getValidBoats();

		// We will put vessels randomly in the grid
		Random random = new Random();

		Set<Character> keys = boats.keySet();

		boolean result;

		// Place all boats
		for (Character character : keys) {
			do {
				result = putVessel(grid, random, boats.get(character), character);
			} while (!result);
		}
		// All vessels have been set, grid is ready

	}

	private void initGrid(char[][] grid) {
		for (int i = 0; i < grid.length; i++) {
			char[] cs = grid[i];
			for (int j = 0; j < cs.length; j++) {
				cs[j] = WATER;
			}
		}
	}

	private boolean putVessel(char[][] grid, Random random, int vesselLength, char vesselId) {
		int row = random.nextInt(10);
		int col = random.nextInt(10);
		if (grid[row][col] != WATER) {
			return false;
		} else {
			if (random.nextBoolean()) {
				if (tryEast(grid, vesselLength, row, col)) {
					putEast(grid, vesselLength, row, col, vesselId);
					return true;
				}
				if (trySouth(grid, vesselLength, row, col)) {
					putSouth(grid, vesselLength, row, col, vesselId);
					return true;
				}
			} else {
				if (trySouth(grid, vesselLength, row, col)) {
					putSouth(grid, vesselLength, row, col, vesselId);
					return true;
				}
				if (tryEast(grid, vesselLength, row, col)) {
					putEast(grid, vesselLength, row, col, vesselId);
					return true;
				}

			}
		}
		return false;
	}

	private boolean tryEast(char[][] grid, int vesselLength, int row, int col) {
		try {
			if (grid[row][col + vesselLength - 1] == WATER) {
				for (int i = col + 1; i < col + vesselLength - 1; i++) {
					if (grid[row][i] != WATER) {
						return false;
					}
				}
				return true;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
		return false;
	}

	private void putEast(char[][] grid, int vesselLength, int row, int col, char vesselId) {
		for (int i = col; i < col + vesselLength; i++) {
			grid[row][i] = vesselId;
		}
	}

	private boolean trySouth(char[][] grid, int vesselLength, int row, int col) {
		try {
			if (grid[row + vesselLength - 1][col] == WATER) {
				for (int i = row + 1; i < row + vesselLength - 1; i++) {
					if (grid[i][col] != WATER) {
						return false;
					}
				}
				return true;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
		return false;
	}

	private void putSouth(char[][] grid, int vesselLength, int row, int col, char vesselId) {
		for (int i = row; i < row + vesselLength; i++) {
			grid[i][col] = vesselId;
		}
	}

	/**
	 * Write the content of <code>inputMatrix</code> to a file referenced by
	 * <code>path</code>
	 * 
	 * @param inputMatrix
	 * @param path
	 * @throws IOException
	 */
	public void fromMatrixToFile(char[][] inputMatrix, String path) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path));
		for (int i = 0; i < inputMatrix.length; i++) {
			StringBuilder stringBuilder = new StringBuilder();
			char[] ds = inputMatrix[i];
			stringBuilder.append(ds[0]);
			for (int j = 1; j < ds.length; j++) {
				char d = ds[j];
				stringBuilder.append(",").append(d);
			}
			bufferedWriter.write(stringBuilder.toString());
			bufferedWriter.newLine();
		}
		bufferedWriter.close();
	}

	public void generateInvalidGridTooLongOrTooShortBoat(char[][] grid) {
		// Fill grid with WATER!
		initGrid(grid);

		// We will put vessels randomly in the grid
		Random random = new Random();

		Map<Character, Integer> boats = getValidBoats();

		Set<Character> keySet = boats.keySet();
		int nextInt = random.nextInt(keySet.size());
		char toEdit = (char) keySet.toArray()[nextInt];

		// Random add or substract
		if (random.nextBoolean()) {
			Integer integer = boats.get(toEdit);
			boats.put(toEdit, integer + 1);
		} else {
			Integer integer = boats.get(toEdit);
			boats.put(toEdit, integer - 1);
		}

		boolean result;

		for (Character character : keySet) {
			do {
				result = putVessel(grid, random, boats.get(character), character);
			} while (!result);
		}

	}

	public Map<Character, Integer> getValidBoats() {
		Map<Character, Integer> boats = new HashMap<Character, Integer>();
		boats.put('p', 5);
		boats.put('c', 4);
		boats.put('o', 3);
		boats.put('s', 3);
		boats.put('t', 2);
		return boats;
	}

	// TODO
	private boolean putVesselInEmptySpaces(char[][] grid, Random random, int vesselLength, char vesselId) {
		int row = random.nextInt(10);
		int col = random.nextInt(10);
		if (grid[row][col] != WATER) {
			return false;
		} else {
			if (random.nextBoolean()) {
				if (tryEastEmptySpaces(grid, vesselLength, row, col)) {
					putEastEmptySpaces(grid, vesselLength, row, col, vesselId);
					return true;
				}
				if (trySouthEmptySpaces(grid, vesselLength, row, col)) {
					putSouthEmptySpaces(grid, vesselLength, row, col, vesselId);
					return true;
				}
			} else {
				if (trySouthEmptySpaces(grid, vesselLength, row, col)) {
					putSouthEmptySpaces(grid, vesselLength, row, col, vesselId);
					return true;
				}
				if (tryEastEmptySpaces(grid, vesselLength, row, col)) {
					putEastEmptySpaces(grid, vesselLength, row, col, vesselId);
					return true;
				}

			}
		}
		return false;
	}

	private boolean tryEastEmptySpaces(char[][] grid, int vesselLength, int row, int col) {
		int cellTested = 0;
		try {
			while (cellTested < vesselLength) {
				// We already tested first cell for water
				col += 1;
				if (grid[row][col] == WATER) {
					cellTested++;
				}
			}
			return true;
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}

	private boolean trySouthEmptySpaces(char[][] grid, int vesselLength, int row, int col) {
		int cellTested = 0;
		try {
			while (cellTested < vesselLength) {
				// We already tested first cell for water
				row += 1;
				if (grid[row][col] == WATER) {
					cellTested++;
				}
			}
			return true;
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}

	private void putSouthEmptySpaces(char[][] grid, int vesselLength, int row, int col, char vesselId) {
		int cellsDone = 0;
		while (cellsDone < vesselLength) {
			if (grid[row][col] == WATER) {
				grid[row][col] = vesselId;
				cellsDone++;
			}
			row++;
		}
	}

	private void putEastEmptySpaces(char[][] grid, int vesselLength, int row, int col, char vesselId) {
		int cellsDone = 0;
		while (cellsDone < vesselLength) {
			if (grid[row][col] == WATER) {
				grid[row][col] = vesselId;
				cellsDone++;
			}
			col++;
		}
	}

	public void generateInvalidGridOverlappingBoats(char[][] grid) {
		// Potentially, disposing boats in directly next empty space can result in a
		// valid
		// grid, so check and generate one that is invalid
		App checker = new App();

		// So we do it until the grid is considered invalid!
		do {
			// Fill grid with WATER!
			initGrid(grid);

			// We will put vessels randomly in the grid
			Random random = new Random();

			Map<Character, Integer> boats = getValidBoats();

			Set<Character> keySet = boats.keySet();

			boolean result;

			for (Character character : keySet) {
				do {
					result = putVesselInEmptySpaces(grid, random, boats.get(character), character);
				} while (!result);
			}
		} while (checker.checkGrid(grid));

	}

	public void generateInvalidGridSwitchingTwoBoatSizes(char[][] grid) {

		App checker = new App();

		do {
			// Fill grid with WATER!
			initGrid(grid);

			// We will put vessels randomly in the grid
			Random random = new Random();

			Map<Character, Integer> boats = getValidBoats();

			Set<Character> keySet = boats.keySet();

			// We switch letters from 2 boats (can be same size, so need to do again if grid
			// is valid...)
			Object[] array = keySet.toArray();
			char firstBoatChar = (char) array[random.nextInt(keySet.size())];
			int firstBoatLength = boats.get(firstBoatChar);
			char secondBoatChar = (char) array[random.nextInt(keySet.size())];
			int secondBoatLength = boats.get(secondBoatChar);
			boats.put(firstBoatChar, secondBoatLength);
			boats.put(secondBoatChar, firstBoatLength);

			boolean result;

			for (Character character : keySet) {
				do {
					result = putVessel(grid, random, boats.get(character), character);
				} while (!result);
			}
		} while (checker.checkGrid(grid));

	}

	public void generateInvalidGridInvalidBoatChars(char[][] grid) {

		// Fill grid with WATER!
		initGrid(grid);

		// We will put vessels randomly in the grid
		Random random = new Random();

		Map<Character, Integer> boats = getValidBoats();

		Set<Character> keySet = boats.keySet();

		// We switch letters from 2 boats (can be same size, so need to do again if grid
		// is valid...)
		Object[] array = keySet.toArray();
		char firstBoatChar = (char) array[random.nextInt(keySet.size())];
		int firstBoatLength = boats.get(firstBoatChar);
		boats.remove(firstBoatChar);
		boats.put('x', firstBoatLength);

		boolean result;

		for (Character character : keySet) {
			do {
				result = putVessel(grid, random, boats.get(character), character);
			} while (!result);
		}

	}

	public void generateInvalidGridSlightChange(char[][] grid) {

		// Fill grid with WATER!
		initGrid(grid);

		// We will put vessels randomly in the grid
		Random random = new Random();

		Map<Character, Integer> boats = getValidBoats();

		Set<Character> keySet = boats.keySet();
		boolean result;

		// First generate a valid grid
		for (Character character : keySet) {
			do {
				result = putVessel(grid, random, boats.get(character), character);
			} while (!result);
		}

		do {
			result = doSlightChange(grid, random);
		} while (!result);

	}

	private boolean doSlightChange(char[][] grid, Random random) {
		try {
			int row = random.nextInt(10);
			int col = random.nextInt(10);
			if (grid[row][col] == WATER) {
				return false;
			} else {
				char valueToMove = grid[row][col];
				int test = random.nextInt(4);
				switch (test) {
				case 0:
					if (grid[row][col + 1] == WATER) {
						grid[row][col + 1] = valueToMove;
						grid[row][col] = WATER;
						return true;
					}
				case 1:
					if (grid[row][col - 1] == WATER) {
						grid[row][col - 1] = valueToMove;
						grid[row][col] = WATER;
						return true;
					}
				case 2:
					if (grid[row + 1][col] == WATER) {
						grid[row + 1][col] = valueToMove;
						grid[row][col] = WATER;
						return true;
					}
					break;
				case 3:
					if (grid[row - 1][col] == WATER) {
						grid[row - 1][col] = valueToMove;
						grid[row][col] = WATER;
						return true;
					}
					break;
				default:
					break;
				}
			}
			return false;
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}

	public void generateInvalidGridDiagonal(char[][] grid) {

		// Fill grid with WATER!
		initGrid(grid);

		// We will put vessels randomly in the grid
		Random random = new Random();

		Map<Character, Integer> boats = getValidBoats();

		boolean result;

		Object[] array = boats.keySet().toArray();
		int nextInt = random.nextInt(array.length);
		Character randomBoatChar = (Character) array[nextInt];
		do {
			result = putVesselDiagonal(grid, random, boats.get(randomBoatChar), randomBoatChar);
		} while (!result);
		boats.remove(randomBoatChar);

		Set<Character> keySet = boats.keySet();
		// First generate a valid grid
		for (Character character : keySet) {
			do {
				result = putVessel(grid, random, boats.get(character), character);
			} while (!result);
		}
	}

	private boolean putVesselDiagonal(char[][] grid, Random random, int vesselLength, char vesselId) {
		int row = random.nextInt(10);
		int col = random.nextInt(10);
		if (grid[row][col] != WATER) {
			return false;
		} else {
			if (random.nextBoolean()) {
				if (tryDiag1(grid, vesselLength, row, col)) {
					putDiag1(grid, vesselLength, row, col, vesselId);
					return true;
				}
				if (tryDiag2(grid, vesselLength, row, col)) {
					putDiag2(grid, vesselLength, row, col, vesselId);
					return true;
				}
			} else {
				if (tryDiag2(grid, vesselLength, row, col)) {
					putDiag2(grid, vesselLength, row, col, vesselId);
					return true;
				}
				if (tryDiag1(grid, vesselLength, row, col)) {
					putDiag1(grid, vesselLength, row, col, vesselId);
					return true;
				}

			}
		}
		return false;
	}

	private boolean tryDiag1(char[][] grid, int vesselLength, int row, int col) {
		try {
			int i = 1;
			while (i < vesselLength) {
				if (grid[row + i][col + i] != WATER) {
					return false;
				}
				i++;
			}
			return true;
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}

	private void putDiag1(char[][] grid, int vesselLength, int row, int col, char vesselId) {
		int i = 0;
		while (i < vesselLength) {
			grid[row + i][col + i] = vesselId;
			i++;
		}
	}

	private boolean tryDiag2(char[][] grid, int vesselLength, int row, int col) {
		try {
			int i = 1;
			while (i < vesselLength) {
				if (grid[row + i][col - i] != WATER) {
					return false;
				}
				i++;
			}
			return true;
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}

	private void putDiag2(char[][] grid, int vesselLength, int row, int col, char vesselId) {
		int i = 0;
		while (i < vesselLength) {
			grid[row + i][col - i] = vesselId;
			i++;
		}
	}

}
