package exercice_2018_02;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;

public class Utils {

	public static char[][] fromFileToMatrix(String path) throws IOException {
		File inputFile = new File(path);
		List<String> readAllLines = Files.readAllLines(inputFile.toPath(), Charset.forName("UTF-8"));
		String firstLine = readAllLines.get(0);
		int width = firstLine.split("[,]").length;
		int height = readAllLines.size();
		char[][] inputMatrix = new char[height][width];
		int row = 0;
		int col = 0;
		for (String line : readAllLines) {
			String[] fields = line.split("[,]");
			for (String field : fields) {
				inputMatrix[row][col] = field.charAt(0);
				col++;
			}
			row++;
			col = 0;
		}
		return inputMatrix;
	}

	public static void printGrid(char[][] inputMatrix) {
		for (int i = 0; i < inputMatrix.length; i++) {
			char[] ds = inputMatrix[i];
			System.out.print(ds[0]);
			for (int j = 1; j < ds.length; j++) {
				System.out.print(" | " + ds[j]);
			}
			System.out.println();
		}
	}

}
