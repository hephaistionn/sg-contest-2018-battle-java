package exercice_2018_02;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

public class AppTest {

	@Test
	public void test_grid() throws IOException {

		File file = new File("inputs");
		File[] listFiles = file.listFiles();
		List<File> asList = Arrays.asList(listFiles);

		Collections.shuffle(asList);

		AppInterface appRef = new App();
		AppInterface appToTest = new AppCandidat();

		for (File file2 : asList) {
			if (file2.isFile()) {
				char[][] fromFileToMatrix = Utils.fromFileToMatrix(file2.getAbsolutePath());
				boolean appRefCheck = false;
				boolean appCandidatCheck = false;
				try {
					appRefCheck = appRef.checkGrid(fromFileToMatrix);
					appCandidatCheck = appToTest.checkGrid(fromFileToMatrix);
					assertEquals(appRefCheck, appCandidatCheck);
				} catch (Exception e) {
					System.out.println("[LOG] " +e);
					fail();
				} catch (AssertionError e) {
					System.out.println("[LOG] This following grid failed test (should have returned " + appRefCheck + ")");

					for (int i = 0; i < fromFileToMatrix.length; i++) {
						StringBuilder builder = new StringBuilder();
						builder.append("[LOG] ");
						for (int j = 0; j < fromFileToMatrix[i].length; j++) {
							builder.append(fromFileToMatrix[i][j]);
							builder.append(" ");
						}
						String str = builder.toString();
						System.out.println(str);
        			}
					
					fail();
				}
			}
		}

	}
}
